Theoretical question:

1 Explain in your own words what the object method is
What is object?
There are seven  data types in JavaScript. From six of them are called in "primitive", because their values is only a single.
An object can be created with figure brackets {…}. A property is a “key: value” pair,
where key is a string (also called a “property name”)

Example
let human = {
  firstName: "John",
  lastName : "Doe",
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};

The this Keyword
In a function definition, this refers to the owner of the function.
In the example above, this is the person object that owns the fullName function.
In other words, this.firstName means the firstName property of this objec

Accessing Object Methods
We access an object method with the following syntax:

objectName.methodName()
We will typically describe fullName() as a method of the person object, and fullName as a property.
The fullName property will execute (as a function) when it is invoked with ().
